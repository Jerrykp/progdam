/*1. Ejercicio FECHAS-HORAS

Para resolver el siguiente ejercicio utiliza la documentación de la API de Java.

Realiza un programa que se ejecute de la forma:
    
    java programa dia mes año Unidad

y, haciendo uso de las clases LocalDate, LocalTime, LocalDateTime, etc. muestre el tiempo transcurrido entre la fecha indicada en los parámetros de ejecución y el instante de la ejecución en la unidad indicada (Segundos, Días o Años). El programa habrá de comprobar que la fecha indicada no pertenezca al futuro.

Ejemplos de ejecución:

    java programa 26 5 2050 Segundos

   , generará la salida

        Fecha futura (no válida)

     java programa 13 8 1975 Años

    , generará la salida

        Han transcurrido xx años*/

import java.util.*;

public class ej1
{
	public static void main (String args[])
	{
		int day, month, year, seconds, minutes, hours, days, months, years;
		String unit;

		
		try
		{
			day = Integer.valueOf(args[0]);
			month = Integer.valueOf(args[1]);
			year = Integer.valueOf(args[2]);
			unit = args[3].toLowerCase();
		}
		catch (Exception e)
		{
			day = 0;
			month = 0;
			year = 0;
			unit = null;

			System.out.println("Falta algún parámetro");
			System.exit(0);
		}
	// le restamos una unidad al mes porque los meses son de 0 a 11
		GregorianCalendar date = new GregorianCalendar(year, month-1, day);
		GregorianCalendar today = new GregorianCalendar();
		//GregorianCalendar today2 = new GregorianCalendar();
		//today.add(Calendar.MONTH, +1);


		if (date.compareTo(today)>0)
			System.out.println("La fecha es futura");
		else
		{
			days = (today.get(Calendar.YEAR)-date.get(Calendar.YEAR))*365+(today.get(Calendar.DAY_OF_YEAR)-date.get(Calendar.DAY_OF_YEAR));
			hours = days*24;
			minutes = hours*60;
			seconds = minutes*60;

			months = (today.get(Calendar.YEAR)-date.get(Calendar.YEAR))*12+(today.get(Calendar.MONTH)-date.get(Calendar.MONTH));
			if (today.get(Calendar.DAY_OF_YEAR) >= date.get(Calendar.DAY_OF_YEAR))
				years = (today.get(Calendar.YEAR)-date.get(Calendar.YEAR));
			else
				years = (today.get(Calendar.YEAR)-date.get(Calendar.YEAR)) - 1;
			switch (unit)
			{
				case "seconds": System.out.println("Han transcurrido "+seconds+" segundos");break;
				case "minutes": System.out.println("Han transcurrido "+minutes+" minutos");break;
				case "hours": System.out.println("Han transcurrido "+hours+" horas");break;
				case "days": System.out.println("Han transcurrido "+days+" días");break;
				case "months": System.out.println("Han transcurrido "+months+" meses");break;
				case "years": System.out.println("Han transcurrido "+years+" años");break;
				default: System.out.println("Unidad no encontrada");break;
			}
		
		
		}
	}
}
