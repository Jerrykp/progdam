/* 
 	Realiza un programa que utilice las clases del paquete anterior:
 - crea un objeto MotorElectrico y hazlo girar y acelerar dos veces.
 - crea un segundo MotorElectrico, copia superficial del anterior.
 - crea un nuevo objeto MotorElectrico con referencia de tipo Motor. Hazlo acelerar. */
 
import motores.*;

public class Ejercicio6 {

	public static void main(String[] args) {
		
		MotorElectrico electrico = new MotorElectrico(30,"125CA"); // velocidad inicial
		electrico.acelera();
		electrico.acelera();
		System.out.println(electrico);
		
		MotorElectrico electrico2 = electrico; // copia superficial
		
		Motor electrico3 = new MotorElectrico(30,"12cc"); // tercer objeto con referencia a la clase base Motor
		electrico3.acelera(); // aunque la referencia sea de la clase base, accede al método acelera() sobreescrito en la clase derivada(velocidad+=10)
		System.out.println(electrico3); 
		

	}

}
