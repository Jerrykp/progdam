// Ejemplo de parámetro pasado por referencia (la función trabaja con el dato original). En este caso, pasamos un dato básico en un array de un único elemento (referencia)
import java.util.Scanner;

public class pasoPorReferencia
{
	public static void main(String[] args) {
		double n;
		Scanner ent = new Scanner(System.in);
		double nums[] = new double[1];
		
		System.out.println("Introduce un número:");
		n = ent.nextDouble();
		nums[0] = n;	// coloco el valor de tipo básico en el array
		doble(nums);
		System.out.println("El valor duplicado es " + nums[0]);
				
		System.exit(0);
	}
	
	public static void doble(double x[])
	{
		x[0] = 2*x[0];
		//System.out.println("El valor duplicado en la función es " + x);
	}
}
