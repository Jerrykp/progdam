import java.util.Date;

class dates
{
	public static void main(String[] args) {
		// se crea un objeto de tipo Date con los milisengundos transcurridos desde el primer instante de 1970
		Date d=new Date();
		System.out.println("It's "+d.toString());
		System.out.println("Han pasado "+d.getTime()+" milisegundos desde la ERA");
		System.out.println("Convirtiendo a 2017...");
		d.setYear(117);	// 1900 + 117
		System.out.println("It's "+d.toString());

		System.exit(0);
	}
}
