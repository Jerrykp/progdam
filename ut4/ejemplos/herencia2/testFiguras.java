class testFiguras
{
	public static void main(String args[])
	{
		cuadrado c = new cuadrado(5);
		circulo ci = new circulo(43);
		rectangulo r = new rectangulo(32,54);
		c.setColor("Verde");ci.setColor("Amarillo");r.setColor("Rojo");
		System.out.println("Cuadrado - Color: " + c.getColor() + ", Área: " + c.getArea());
		System.out.println("Círculo - Color: " + ci.getColor() + ", Área: " + ci.getArea());
		System.out.println("Rectángulo - Color: " + r.getColor() + ", Área: " + r.getArea());
	}
}
