class padre{
	protected int dato1, dato2;
	padre(int x,int y){dato1 = x; dato2 = y;}
	padre(){
		this(5,5);	// equivale a dato1=5;dato2=5 pero llamando al constructor anterior, el de dos parámetros
	}
}

class hijo extends padre{
	private int dato1, dato2;
	hijo(int x,int y){
		super(2,2);
		dato1 = x; 
		dato2 = y;
	}
	hijo(){
		//super(); Esta llamada se realiza implícitamente
		dato1 = 3; 
		dato2 = 3;
	}
	public void getDato(){
		System.out.println("Padre dato1:" + super.dato1);
		System.out.println("Padre dato2:" + super.dato2);
		System.out.println("hijo dato1:" + this.dato1);
		System.out.println("hijo dato2:" + this.dato2);
	}
}

class superconstructor {
	public static void main(String[] args) {
		hijo h1 = new hijo(1,6);
		h1.getDato();
		hijo h2 = new hijo();
		h2.getDato();
	}
}

