interface OperacionesFigura
{
	public float area();
}

class Figura
{
	protected String color;
	protected int grosorLinea;

	public Figura()
	{
		System.out.println("Construyendo un objeto Figura");
		color="negro";
		grosorLinea=1;
	}
	public Figura(String c,int g)
	{
		System.out.println("Construyendo un objeto Figura con el segundo constructor");
		color=c;
		grosorLinea=g;
	}
	public void setColor(String c) { color=c; }
	public void setGrosorLinea(int g) { grosorLinea=g; }
	public String getColor() { return color; }
	public int getGrosorLinea() { return grosorLinea; }
}

class Rectangulo extends Figura implements OperacionesFigura
{
	protected float lado1;
	protected float lado2;

	public Rectangulo() 
	{
		System.out.println("Construyendo un Rectángulo con el primer constructor.");
	 	lado1=lado2=0;
	 }
	public Rectangulo(float l1,float l2)
	{
		lado1=l1; lado2=l2;
	}
	public Rectangulo(float l1,float l2,String c, int g)
	{
		super(c,g);
		lado1=l1; lado2=l2;
	}
	public Rectangulo(Rectangulo r)
	{
		this.lado1=r.lado1;
		this.lado2=r.lado2;
	}
	public void setLado1(float l) { lado1=l; }
	public void setLado2(float l) { lado2=l; }
	public float getLado1() { return lado1; }
	public float getLado2() { return lado2; }
	public float area() { return lado1*lado2; }
}

class Cuadrado extends Figura implements OperacionesFigura
{
	protected float lado;
	
	public Cuadrado() { lado=0; }
	public Cuadrado(float l)
	{
		lado=l;
	}
	public Cuadrado(Cuadrado c)
	{
		this.lado=c.lado;
	}
	public void setLado(float l) { lado=l; }
	public float getLado() { return lado; }
	public float area() { return lado*lado; }
}

class Circulo extends Figura implements OperacionesFigura
{
	protected float radio;
	
	public Circulo() { radio=0; }
	public Circulo(float r)
	{
		radio=r;
	}
	public Circulo(Circulo c)
	{
		this.radio=c.radio;
	}
	public void setRadio(float r) { radio=r; }
	public float getRadio() { return radio; }
	public float area() { return (float) (Math.PI)*radio*radio; }
}

class herencia
{
	public static void main(String[] args) {
		Rectangulo r1=new Rectangulo();
		r1.setLado1(2.0F); r1.setLado2(3.0F);
		Rectangulo r2=new Rectangulo(1,2,"verde",8);
		System.out.println("El segundo rectángulo se ha creado con color "+r2.getColor()+" y su grosor de línea es de "+r2.getGrosorLinea());
		Rectangulo r3=new Rectangulo(r2);
		System.out.println("El tercer rectángulo tiene lados "+r3.getLado1()+ " y "+r3.getLado2()+" y su área es de "+r3.area()+".");
		Cuadrado c1=new Cuadrado();
		c1.setLado(2.0F);
		Cuadrado c2=new Cuadrado(1);
		Cuadrado c3=new Cuadrado(c2);
		System.out.println("El tecer cuadrado tiene lado "+c3.getLado()+ " y su área es de "+c3.area()+".");
		Circulo ci1=new Circulo();
		ci1.setRadio(2.0F);
		Circulo ci2=new Circulo(1);
		Circulo ci3=new Circulo(ci2);
		System.out.println("El tecer círculo tiene radio "+ci3.getRadio()+ " y su área es de "+ci3.area()+".");
		
		System.exit(0);
	}
}
