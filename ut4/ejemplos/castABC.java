class A {
	protected int a;
	public void ma() { a++; }
}

class B extends A {
	protected int b;
	public void mb() { b++; }
}

class C extends A {
	protected int c;
	public void mc() { c++; }
}

public class castABC
{
	public static void main(String[] args) {
	B ob = new B();
	C oc = new C();
	((A)ob).ma();
	((A)oc).ma();

	//((C)ob).ma();	// provoca error de compilación: no puedo hacer CAST
	//((B)oc).ma();
	
	((C)(A)ob).ma();	// provoca error de ejecución, pero no de compilación
	((B)(A)oc).ma();
	
		System.exit(0);
	}	
}
