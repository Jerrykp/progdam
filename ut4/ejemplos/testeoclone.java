// DA ERRORES DE COMPILACIÓN. A CORREGIR POR EL ALUMNO

class rectangulo implements Cloneable
{
	private int ancho;
	private int alto;
	private String nombre;
	
	public rectangulo(int an,int al) { ancho=an; alto=al; }
	public void incrementarAncho() { ancho++; }
	public void incrementarAlto() { alto++; }
	public void setNombre(String s) { nombre=s; }
	public int getAncho() { return ancho; }
	public int getAlto() { return alto; }
	public String getNombre() { return nombre; }
	public Object clone()
	{
        	Object objeto=null;
        	try{
            	objeto =super.clone();
        	}
        	catch(CloneNotSupportedException ex){
            System.out.println(" Error al duplicar");
        	}
	        return objeto;
	}
}

public class testeoclone
{
	public static void main(String[] args)
	{
		rectangulo r1 = new rectangulo(5,7);
		rectangulo r2 = (rectangulo) r1.clone();
		r2.incrementarAncho();
		r2.incrementarAlto();
		r1.setNombre("Chiquito");
		r2.setNombre("Grande");
		System.out.println("Alto: "+r1.getAlto());
		System.out.println("Ancho: "+r1.getAncho());
		System.out.println("Alto: "+r2.getAlto());
		System.out.println("Ancho: "+r2.getAncho());
		System.out.println("Nombre: "+r1.getNombre());
		System.out.println("Nombre: "+r2.getNombre());
    	}
}

