import java.util.StringTokenizer;

public class ej3
{
	public static double[]	lnotas	= new double[4];

	public static void main(String[] args)
	{
		int i = 0;

		StringTokenizer notas;
		notas = new StringTokenizer("Juan Carlos\n8.5\nAndrés\n4.9\n Pedro\n3.8\nJuan\n6.3", "\n");

		while (notas.hasMoreTokens())
		{
			String nombre = notas.nextToken();
			String nota = notas.nextToken();
			lnotas[i] = Double.valueOf(nota).doubleValue();
			System.out.println("El alumno " + nombre + " ha sacado un " + nota);
			i++;
		}
		
		for (i = 0; i < 4; i++)
			System.out.println("Notas: " + lnotas[i]);
	}
}
