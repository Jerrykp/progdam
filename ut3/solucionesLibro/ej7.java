public class ej7
{
	final static int POS = 50;	// número de posiciones del array
	final static int LIMITE	= 100;	// Números entre 1..Límite

	public static int getaleatorio()
	{
		return (int) (Math.random() * LIMITE + 1);

	}

	public static int[] combina(int[] lista1, int[] lista2)
	{
		int[] l = new int[lista1.length + lista2.length];
		int i = 0;

		for (int j = 0; j < lista1.length; j++)
		{
			l[i] = lista1[j];
			i++;
		}

		for (int j = 0; j < lista2.length; j++)
		{
			l[i] = lista2[j];
			i++;
		}
		ordena(l);
		return l;
	}

	public static void ordena(int array[])
	{
		int aux;

		boolean cambio;

		for (int i = array.length; i > 0; i--)
		{
			cambio = false;
			for (int j = 0; j < i - 1; j++)
			{
				if (array[j] > array[j + 1])
				{
					aux = array[j + 1];
					array[j + 1] = array[j];
					array[j] = aux;
					cambio = true;
				}
			}
			if (!cambio)
				return;
		}
	}

	public static void muestra(int array[])
	{
		for (int i=0;i<array.length;i++)
			System.out.print(array[i]+" ");
	 }

	public static void main(String[] args)
	{
		int[] lista1 = new int[POS];
		int[] lista2 = new int[POS];
		int[] ltot = new int[2*POS];
			
		for (int i=0;i<POS;i++)
		{
			lista1[i]=getaleatorio();
			lista2[i]=getaleatorio();
		}
		System.out.println("");
		ltot=combina(lista1,lista2);
		muestra(ltot);//se muestra el vector ordenado
		System.out.println("");
	 }
}
