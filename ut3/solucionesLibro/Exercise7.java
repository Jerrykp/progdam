/*
  7.- Realiza un método que tome como parámetros de entrada dos arrays de enteros y devuelva como salida un único array con los elementos de los anterioriores de forma ascedente.
*/


import java.util.Scanner;


public class Exercise7{


	final static int SIZE = 7;
	

	public static void main(String args[]){


		Scanner sc = new Scanner(System.in);
		int v1[] = new int[SIZE];
		int v2[] = new int[SIZE];
		int finalVector[] = new int[SIZE * 2];
		int choice;


		System.out.println("\nDados dos vectores de siete números enteros generados aleatoriamente, el programa los unificará en un único array, que imprimirá en consola, ordenados sus elementos de menor a mayor.");
		System.out.print("\nPulse 1 para generar los vectores de forma aleatoria o 2 si prefiere elegir sus elementos: ")		;
		choice = sc.nextInt();

		while(choice != 1 && choice != 2){
			System.out.printf("\nError => '%d' no es una entrada válida.\n",choice);
			System.out.print("\nPulse 1 para generar los vectores de forma aleatoria o 2 si prefiere elegir sus elementos: ");
			choice = sc.nextInt();
		}

		if(choice == 1){

			for(int i=0; i<SIZE; i++){
				v1[i] = (int)(1 + 100 * Math.random());
				v2[i] = (int)(1 + 100 * Math.random());
			}


			System.out.print("\nPrimer vector:");

			for(int i=0; i<SIZE; i++)
				System.out.print("  " + v1[i]);


			System.out.print("\n\nSegundo vector:");

			for(int i=0; i<SIZE; i++)
				System.out.print("  " + v2[i]);

			System.out.println();
		}

		else{
			
			System.out.print("\n\nIntroduzca " + SIZE + " números enteros para componer el primer vector.\n\n");

			for(int i=0; i<SIZE; i++){
				System.out.printf("%dº: ",(i+1));
				v1[i] = sc.nextInt();
			}


			System.out.print("\n\nIntroduzca " + SIZE + " números enteros para componer el segundo vector.\n\n");

			for(int i=0; i<SIZE; i++){
				System.out.printf("%dº: ",(i+1));
				v2[i] = sc.nextInt();
			}
		}


		System.out.println("\n\nUnificados y ordenados los elementos de los vectores anteriores, el resultado es el que se observa a continuación:\n");

		finalVector = concatAndSort(v1,v2);

		for(int i=0; i<finalVector.length; i++)
			System.out.print(" " + finalVector[i]);

		System.out.println("\n\n");
	}


	/*
	  Método de tipo 'int' que recibe dos arrays de enteros,
	  a partir de los cuales crea uno sólo, que devuelve ordenado de menor a mayor.
	*/
	public static int[] concatAndSort(int v1[], int v2[]){

		//int v[] = new int[SIZE * 2];
		int i,v[] = new int[v1.length + v2.length];
		boolean sorted = false;
		int aux, lmt = v1.length + v2.length;

		for(i=0; i < v1.length; i++)
			v[i] = v1[i];

		for( ; i < v1.length + v2.length; i++)
			v[i] = v2[i-v1.length];

		//Ordenación del vector compuesto utilizando el método de la burbuja optimizado.
		for(i=lmt-2; i>=0 && !sorted; i--){

			sorted = true;

			for(int j=0; j<=i; j++)
				if(v[j] > v[j+1]){
					sorted = false;
					aux = v[j];
					v[j] = v[j+1];
					v[j+1] = aux;
				}
		}

		return v;
	}
}
