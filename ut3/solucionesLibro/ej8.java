public class ej8
{
	private static int[]	lista;
	final static int		POS		= 1000; // número de posiciones del array
	final static int		LIMITE	= 1000; // Números entre 1..Límite

	public static int getaleatorio()
	{

		return (int) (Math.random() * LIMITE + 1);
	}

	public static void ordena(int array[])
	{

		int aux;

		for (int i = array.length; i > 0; i--)
		{

			for (int j = 0; j < i - 1; j++)
			{

				if (array[j] > array[j + 1])
				{

					aux = array[j + 1];

					array[j + 1] = array[j];

					array[j] = aux;

				}

			}

		}
	}

	public static void muestra()
	{

		for (int i = 0; i < POS; i++)
		{

			System.out.print(lista[i] + " ");

		}
	}

	public static void muestra10(){
			
	System.out.println("Se mostrarán los mayores números generados (orden decreciente)");
			
	for (int i=POS-1;i>=POS-10;i--){
				
	System.out.print(lista[i]+" ");
			
	}
		 }

	public static void main(String[] args)
	{

		lista = new int[POS];

		for (int i = 0; i < POS; i++)
		{

			lista[i] = getaleatorio();

		}

		muestra();// se muestra el vector desordenado

		System.out.println("");

		ordena(lista); // ordenación por burbuja

		System.out.println("");

		muestra10();// se muestran los 10 mayores

		System.out.println("");
	}
}
