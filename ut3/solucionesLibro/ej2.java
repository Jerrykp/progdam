public class ej2
{
	private static int[] lista;
	private static int[] lista2;
	final static int POS = 100;	// número de posiciones del array
	final static int LIMITE	= 100;

	public static int getaleatorio()
	{
		return (int) (Math.random() * LIMITE + 1);

	}

	public static void muestra(int cuantos)
	{ // 1 el primero ; 2 el segundo; 0: los dos
		if (cuantos == 1 || cuantos == 0)
			for (int i = 0; i < POS; i++)
				System.out.print(lista[i] + " ");
		if (cuantos == 2 || cuantos == 0)
			for (int i = 0; i < POS; i++)
				System.out.print(lista2[i] + " ");
	}

	public static void crea_segundo()
	{
		for (int i = 0; i < POS; i++)
			lista2[i] = 0;

		for (int i = 0; i < POS; i++)
		{ // recorriendo el primer array
			int pos = -1;

			for (int j = 0; j < POS; j++)
			 // obteniendo la posición
				if (pos == -1 && (lista2[j] == 0 || lista2[j] > lista[i]))
					pos = j;

			for (int k = POS - 2; k >= pos; k--)
				lista2[k + 1] = lista2[k];
			
			lista2[pos] = lista[i];
		}
	}

	public static void main(String[] args)
	{
		lista = new int[POS];
		lista2 = new int[POS];

		for (int i = 0; i < POS; i++)
			lista[i] = getaleatorio();

		muestra(1);// se muestra el vector desordenado
		System.out.println("\n\n");
		System.out.println("Ahora el segundo vector ordenado");
		crea_segundo();
		muestra(2);// se muestra el vector ordenado
	}
}
