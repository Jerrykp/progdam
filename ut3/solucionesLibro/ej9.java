public class ej9
{
	private static int[]	lista;
	final static int POS = 100;	// número de posiciones del array
	final static int LIMITE	= 70;

	public static int getaleatorio()
	{

		return (int) (Math.random() * LIMITE + 11);
	}

	public static void ordena(int array[])
	{

		int aux;

		for (int i = array.length; i > 0; i--)
		{
			for (int j = 0; j < i - 1; j++)
			{
				if (array[j] > array[j + 1])
				{
					aux = array[j + 1];
					array[j + 1] = array[j];
					array[j] = aux;
				}
			}
		}
	}

	public static void muestra()
	{
		for (int i = 0; i < POS; i++)
			System.out.print(lista[i] + " ");
	}

	public static double media()
	{

		double suma = 0;

		for (int i = 0; i < POS; i++)
			suma += lista[i];
		suma /= 100;

		return suma;
	}

	public static int masrepe()
	{

		final int LIM = 80;

		int[] repes = new int[LIM];

		for (int i = 0; i < LIM; i++)
			repes[i] = 0;
		
		for (int i = 0; i < LIM; i++)
			repes[lista[i]]++;
		
		int mayor = 0;

		for (int i = 0; i < LIM; i++)
			if (repes[i] > mayor)
				mayor = repes[i];

		for (int i = 0; i < LIM; i++)
			if (repes[i] == mayor)
				return i;

		return 0;
	}

	public static void main(String[] args)
	{

		lista = new int[POS];

		for (int i = 0; i < POS; i++)
		{

			lista[i] = getaleatorio();

		}

		muestra();// se muestra el vector desordenado

		System.out.println("");

		ordena(lista); // ordenación por burbuja

		muestra();// se muestra el vector ordenado

		System.out.println("");

		System.out.println("");

		System.out.println("");

		System.out.println("El mayor es: " + lista[POS - 1]
				+ " y el menor es " + lista[0]);

		System.out.println("La media es: " + media());

		System.out.println("El valor más repetido es: " + masrepe());
		System.out.println("");
	}
}
