/*Realiza un programa que, mediante la clase Hashtable, permita gestionar 
películas de una videoteca o videoclub. Para ello, se utilizarán entradas 
de tipo <Integer,String> tal que cada película tenga un número de película y su título.

	El programa permitirá:
	- introducir películas desde teclado
	- listar todas las películas
	- eliminar una película a partir de su número
	- consultar el título de una película a partir de su número	*/
	
//import java.util.Hashtable;
import java.util.*;


public class SolucionHashTableVideoclub{

	public static void main(String args[]){
		int lista=1; String pelicula="i"; 
		Hashtable<Integer,String> videoclub = new Hashtable<Integer,String>(); //creo el hashTable

		videoclub.put(1,"Matrix");
		videoclub.put(8,"Pulp Fiction");
		videoclub.put(4,"Trainspotting");

		System.out.println(videoclub.get(4)); //imprimira el String que contenga el nº 4, en este caso Trainspotting
		System.out.println(videoclub); 	//imprimira todo el contenido de el Hashtable (implícitamente llama al método toString() de Hashtable

		
		int pase=1;
		while (pase == 1){	//Introducir películas con su Nº desde teclado hasta salir del bucle
		
			System.out.println("Introduce Nº de película: ");
			lista = Integer.parseInt(System.console().readLine());
		
			System.out.println("Introduce la película número " + lista + ": ");
			pelicula = System.console().readLine();

			videoclub.put(lista,pelicula);

			System.out.println("¿Quieres introducir más datos en el hashTable? (pulsa 1 si lo deseas, o cualquier otro número si no)" );
			pase = Integer.parseInt(System.console().readLine());
		}
		
		listaHash(videoclub);
		System.out.println("¿Qué número de película quieres eliminar?");
		lista = Integer.parseInt(System.console().readLine());
		deleteHash(lista,videoclub);

		System.out.println(videoclub); 
		System.out.println("Introduce el número de la pelicula a consultar");
		lista = Integer.parseInt(System.console().readLine());
		System.out.println("Esa película es " + consulta(lista,videoclub));

		listaHash(videoclub);

		recorreHash(videoclub);

	}


	public static void listaHash(Hashtable tabla){
		System.out.println("El tamaño actual de la lista es: " + tabla.size() + " y contiene: " + tabla); //.size dirá el número de elementos (valor con su película)que hay en la lista.
		
	}
	
	public static void deleteHash(int num,Hashtable tabla){
		tabla.remove(num);
	}
	
	public static String consulta(int con,Hashtable tabla){
		
		return (String)(tabla.get(con));
	}

	public static void recorreHash(Hashtable tabla){

		Enumeration claves = tabla.keys(); //tenemos que crear un Enumaration del hashTable de keys y elements para poder recorrer la tabla, necesita importar otra clase
		Enumeration valores = tabla.elements();
		while(claves.hasMoreElements()){
			System.out.println("Número:" + claves.nextElement() + " película: " + valores.nextElement());

		}
	}
}
