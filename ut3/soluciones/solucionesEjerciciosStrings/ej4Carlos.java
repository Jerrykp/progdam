//	4. Implemente un programa que reciba una cadena S y una letra X, y coloque en mayúsculas cada ocurrencia de X en S. (la función debe modificar la variable S).
import java.util.*;
public class ej4Carlos
{
	public static void main(String[] args)
	{
		Scanner ent = new Scanner(System.in);

		System.out.println("Introduce un texto en minúsculas:");
		String s = ent.nextLine();

		System.out.println("Introduce un caracter:");
		char c = ent.nextLine().charAt(0);

		System.out.println("El texto es: " + s + " , el caracter es: " + c);

		String s2 = null;

/*		for (int i=0; i < s.length(); i++){
			if ((s.charAt(i) == c))
				// comprueba si es minúscula
				if ((s.charAt(i) >= 97) && (s.charAt(i) <= 122))
					s2 = s.replace(c,(char)(c-32));
		}		*/
		s2 = s.replace(c,(char)(c-32));
		System.out.println(s2);	
	}
}
