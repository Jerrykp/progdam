/*3. Realiza un programa en Java que haga lo siguiente (solución preferentemente modular, todo en una misma clase que incluya main y el resto de funciones):

- genere 100 números enteros aleatorios, entre 1 y 100, y los cargue en un array de 100 enteros
- muestre el array por pantalla
- pida al usuario un valor (entre 1 y 100 también) e indique si, o no, se encuentra en el array (búsqueda lineal en vector desordenado)
- ordene el array con el método de la burbuja
- vuelva a mostrar el array
- pida otro valor, entre 1 y 100, al usuario
- realice otra búsqueda (ahora dicotómica) indicando, igualmente, si se encuentra, o no, en el array
- con el mismo valor de búsqueda, finalmente realizará una búsqueda lineal indicando cuántas veces ha aparecido el valor en el array*/

import java.util.Scanner;

public class ej3{

	static Scanner ent = new Scanner(System.in);
	private static int numeros[] = new int[100];

	public static void main( String [] args ){

		int num;
		
		carga();
		muestra();
		System.out.println("*Búsqueda lineal\nIntroduce un número: ");
		num=ent.nextInt();

		if (busq_lineal(num))
			System.out.println("SI está en el array");
		else
			System.out.println("NO está en el array");

		ordena();
		System.out.println("\nArray ordenado: ");
		muestra();

		System.out.println("*Búsqueda dicotómica\nIntroduce un número: ");
		num=ent.nextInt();

		if (busq_dicotomica(num))
			System.out.println("SI está en el array");
		else
			System.out.println("NO está en el array");

		System.out.println("Se repite " + repeticiones(num) + " veces.");
	}

	public static void carga(){

		//int num;
		for ( int i=0 ; i < numeros.length ; i++ ) {
			numeros[i] = (int)(100 * Math.random() + 1);
			// = num;	
		}
	}

	public static void muestra(){

		for ( int i=0 ; i<numeros.length ; i++ )
			System.out.print("[" + numeros[i] + "] ");

		System.out.println("\n");
	}

	public static void ordena(){

		int aux=0; boolean ordenado=false;

		for ( int i = numeros.length - 2  ; (i >= 0) && (!ordenado) ; i-- ) {
			ordenado=true;
			for ( int j=0 ; j <= i ; j++ ) {
				if(numeros[j] > numeros[j+1]){
					ordenado=false;
					aux=numeros[j];
					numeros[j]=numeros[j+1];
					numeros[j+1]=aux;
				}
			}
		}
		System.out.println();		
	}

	public static boolean busq_lineal(int num){

		for ( int i=0 ; i < numeros.length ; i++ )
			if( num == numeros[i])
				return true;
		return false;
	}

	public static boolean busq_dicotomica(int num){

		int izq=0, der=numeros.length - 1, centro=(der / 2);

		while ((numeros[centro] != num) && (izq<=der)) {
			if(numeros[centro] < num)
				izq = centro + 1;
			else
				der = centro - 1;
			centro=(izq+der)/2;	
		}

		if (numeros[centro] == num)	// if (izq > der) return false; return true
			return true;
		//else
			return false;
	}

	public static int repeticiones(int num){
		int cont=0;

		for ( int i=0 ; i<numeros.length ; i++ )
			if( num == numeros[i])
				cont++;

		return cont;
	}

}
