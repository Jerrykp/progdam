//	Crear un array de objetos Futbolista(nombre,goles) y ordenar el array (con el método de la burbuja) para que éste sea la tabla de máximos goleadores, ordenados de mayor a menor número de goles. El programa acabará mostrando esa clasificación.

import java.util.Scanner;

class Futbolista {

	private int goles;
	private String nombre;

	// Constructs
	public Futbolista() {
		nombre = "";
		goles = 0;
	}
	public Futbolista (String nombre, int goles) {
		this.nombre = nombre;
		this.goles = goles;
	}

	// Setters
	public void setNombre (String nombre) {
		this.nombre = nombre;
	}
	public void setGoles (int goles) {
		this.goles = goles;
	}

	// Getters
	public String getNombre() {
		return nombre;
	}
	public int getGoles() {
		return goles;
	}
	public String toString() { return nombre + " ha marcado " + goles + " goles.";
	}
}

public class ej1 {
	
	public static void main (String args[]) {
		final int TAM = 5;
		int goles;
		String nombre;
		Futbolista fut[] = new Futbolista[TAM];
		Scanner ent = new Scanner (System.in);
		boolean ordenado = false;

		// Cargar el array
		// Ocurre que si después de leer desde teclado un caracter numérico
		// se lee un caracter alfanumérico, tomará el enter de buffer y pasará
		for (int cont = 0; cont < TAM; cont++) {
			System.out.print ("\nIntroduce el nombre del jugador: ");
			nombre = ent.nextLine();
			System.out.print ("Introduce su número de goles: ");
			goles = ent.nextInt();

			fut[cont] = new Futbolista (nombre, goles);
			ent.nextLine(); // Limpiar el buffer del teclado
			//System.out.println ("");
		}

		// Ordenar por el método de la burbuja
		for (int tope = TAM - 2; (tope >= 0) && (!ordenado) ; tope--) {
			ordenado = true;
			for (int cont = 0; cont <= tope; cont++)
				if (fut[cont].getGoles() < fut[cont+1].getGoles()) {
					ordenado = false;
					//System.out.println (fut[cont].getGoles() +"<"+ fut[cont+1].getGoles());
					/*nombre = fut[cont].getNombre();
					goles = fut[cont].getGoles();

					fut[cont].setNombre (fut[cont+1].getNombre());
					fut[cont].setGoles (fut[cont+1].getGoles());

					fut[cont+1].setNombre (nombre);
					fut[cont+1].setGoles (goles);*/
					Futbolista aux = fut[cont];
					fut[cont] = fut[cont+1];
					fut[cont+1] = aux;
			}
		}

		// Mostrar el array
		for (int cont = 0; cont < TAM; cont++) {
//			System.out.println (fut[cont].getNombre() +"\t-\t"
//							+ fut[cont].getGoles());
			System.out.println(fut[cont]);
		}
	}
}
