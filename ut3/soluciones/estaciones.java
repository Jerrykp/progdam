// 25. Escribir un programa que lea las temperaturas obtenidas en 15 observatorios meteorológicos y escriba la temperatura mínima y cuantas mínimas se han producido.

import java.util.Scanner;
public class estaciones
{
    private static final int F = 15; // numero de filas

    public static void main(String args[]) 
    {
        double temperatura[] = new double [F]; // array tamaño F

        System.out.println("La temperatura media ha sido  " + escribirYmediaTemp(temperatura));
        System.out.println("La maxima temperatura ha sido  " + Max(temperatura) + " y la minima " + Min(temperatura));      
  
        System.exit(0);
    }
  
    public static double escribirYmediaTemp(double n[])
    {
        double suma=0;
        Scanner ent=new Scanner(System.in);
        for(int f=0 ; f < F ; f++)
        {
            System.out.print("Introduce temperatura observatoria numero " + (f+1) + " :\n");
            n[f]=ent.nextDouble();
            suma+=n[f];
        }  
        return suma/F;
    }

    public static double Max(double n[])
    {
        double max=n[0];	// inicialmente el primer valor es máx
        for(int f=1 ; f<F ; f++)
            if(n[f]>max)
                max=n[f];
    
        return max;        
    }

    public static double Min(double n[])
    {
        double min=n[0];
        for(int f=1 ; f<F ; f++)
            if(n[f]<min)
                min=n[f];

        return min;            
    }

}
