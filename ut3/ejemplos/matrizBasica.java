// Ejemplo de MATRIZ de tipo básico (double)

import java.util.Scanner;

public class matrizBasica
{
	private static final int F = 3;	// número de filas
	private static final int C = 4;	// número de columnas
	public static void main(String[] args) {
		double matriz[][] = new double[F][C];	// matriz 3x4, 12 valores
		cargaMatriz(matriz);
		muestraMatriz(matriz);
		for (int i=0 ; i < F ; i++)
			System.out.println("El promedio de la fila " + (i+1) + " es " + promedioFila(matriz[i]));

		System.exit(0);
	}
	
	public static void cargaMatriz(double n[][])
	{
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce " + F + "x" + C + " números:");
		// for externo que me permite avanzar por filas
		for ( int f=0 ; f < F ; f++)
			// for interno que me permite avanzar por columnas
			for ( int c=0 ; c < C ; c++)
				n[f][c] = ent.nextDouble();
	}
	
	public static void muestraMatriz(double n[][])
	{
		System.out.println("Los valores contenidos en la matriz son:");
		// for externo que me permite avanzar por filas
		for ( int f=0 ; f < F ; f++)
		{
			// for interno que me permite avanzar por columnas
			for ( int c=0 ; c < C ; c++)
				System.out.print(n[f][c] + "\t");
			System.out.println("");
		}
			
	}
	// recibe un vector y retorna su promedio
	public static double promedioFila(double nums[])
	{
		double suma=0;
		
		for (int i=0 ; i < C ; i++)
			suma += nums[i];
		return suma/C;
	}		
}
