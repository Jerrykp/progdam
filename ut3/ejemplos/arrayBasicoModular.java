// ejemplo MODULAR de array de tipo básico

import java.util.Scanner;

public class arrayBasicoModular
{
	private final static int TAM = 5;
	
	public static void main(String args[])
	{
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		// guardo en él los primeros múltiplos de 3
		System.out.println("Introduce " + TAM + " números:");
		cargaValores(nums);
		// muestro el contenido del array en pantalla
		muestraValores(nums);	
	}
	
	public static void cargaValores(double n[])
	{
		Scanner ent = new Scanner(System.in);
		for (int i = 0 ; i < TAM ; i++)
			n[i] = ent.nextDouble();
	}
	
	public static void muestraValores(double n[])
	{
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(n[i] + "\t");
		System.out.println("");
	}

}
