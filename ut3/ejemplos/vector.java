import java.util.*;

public class vector
{
	public static void main(String[] args)
	{
		Vector<String> v=new Vector<String>();
		Scanner ent=new Scanner(System.in);
		String s="";
		
		System.out.println("Introduce un nombre:");
		s=ent.nextLine();
		while(!s.isEmpty())
		{
			v.add(s);
			System.out.println("Introduce un nombre:");
			s=ent.nextLine();
		}
		/* Iterator vItr = v.iterator(); 
		
		System.out.println("\nElementos en el vector:"); 
		while(vItr.hasNext()) 
			System.out.println(vItr.next()); */
			
		/* Este tipo de bucle puede recorrer cualquier clase que implemente la interfaz Iterable<E>. */
		for (String nom: v)
			System.out.println(nom);
						
		System.out.println();
	}
}
