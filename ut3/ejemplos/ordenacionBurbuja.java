// Ejemplo de ORDENACIÓN de array con el método de la BURBUJA

import java.util.Scanner;

public class ordenacionBurbuja
{
	private final static int TAM = 10;
	public static void main(String args[])
	{
		Scanner ent = new Scanner(System.in);
		// declaro el array de 5 valores numéricos double
		double aux; /*int comps=0;*/
		double nums[] = new double[TAM];
		// guardo en él los primeros múltiplos de 3
		for (int i = 0 ; i < TAM ; i++)
			//nums[i] = (int)(10*Math.random());
			nums[i] = ent.nextDouble();
		// muestro el contenido del array en pantalla
		System.out.println("El contenido del array es");
		for (int i = 0 ; i < TAM ; i++)
			System.out.println(nums[i]);
		// ORDENO EL ARRAY
		for(int tope = TAM -2 ; tope >= 0 ; tope--)
		{
			System.out.println("Empezando desde la primera posición ...");
			for(int i=0 ; i <= tope ; i++)
				// si es mayor que su siguiente, invierto ambos valores
				if (nums[i] > nums[i+1])
				{
					aux = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = aux;
				}
		}
		// muestro el contenido del array en pantalla YA ORDENADO
		System.out.println("El contenido del array es");
		for (int i = 0 ; i < TAM ; i++)
			System.out.println(nums[i]);
		//System.out.println("Hemos ordenado utilizando " + comps + " comparaciones.");
		
		
			
	}
}
