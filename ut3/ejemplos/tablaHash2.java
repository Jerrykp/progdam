import java.util.*;

public class tablaHash2 { 
    public static void main(String[] args) { 
	Hashtable<String,String> dic  = new Hashtable<String,String>();

	dic.put("HOLA","HELLO");
	dic.put("ADIOS", "BYE");
	dic.put("MESA","TABLE");
	dic.put("SILLA","CHAIR");
	dic.put("CABEZA","HEAD");
	dic.put("CARA","FACE");
//	dic.put("CARA","EXPENSIVE");	sustituiría la entrada anterior para CARA
	String saludo  = dic.get("HOLA");
	String despedida  = dic.get("ADIOS");
	String brazo = dic.get("BRAZO");
	String s = dic.get("A");	// s quedará a NULL
//	String s2 = dic.get(s);		// lanzaría una excepción

	System.out.println("HOLA  : " + saludo);//muestra HELLO por pantalla
	System.out.println("ADIOS : " + despedida);//muestra BYE por pantalla
	System.out.println("BRAZO : " + brazo); //muestra null por pantalla

	System.out.println("dic contiene " + dic.size() + " pares."); 
	
	if( dic.containsKey("HOLA") ){
		System.out.println("dic contiene HOLA como clave");
	}else{
		System.out.println("dic NO contiene HOLA como clave");
	}
	
	if( dic.contains("HELLO") ){
		System.out.println("dic contiene HELLO como valor");
	}else{
		System.out.println("dic NO contiene HELLO como valor");
	}
	
	System.out.println("Mostrando todas las claves de la tabla hash...");
	Enumeration k = dic.keys();
	while( k.hasMoreElements() )
		System.out.println( k.nextElement() );

	System.out.println("Mostrando todos los valores de la tabla hash...");
	Enumeration e = dic.elements();
	while( e.hasMoreElements() )
		System.out.println( e.nextElement() );

	System.out.println( "Eliminando el dato " + dic.remove("HOLA"));
	
	System.out.println("dic contiene " + dic.size() + " pares.");
    }
}	
