// Primer ejemplo de array de tipo básico

public class arrayBasico
{
	private final static int TAM = 5;
	public static void main(String args[])
	{
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		// guardo en él los primeros múltiplos de 3
		for (int i = 0 ; i < TAM ; i++)
			nums[i] = 3*(i+1);
		// muestro el contenido del array en pantalla
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
			
	}
}
