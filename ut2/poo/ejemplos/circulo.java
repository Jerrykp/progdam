// Clase instanciable circulo

public class circulo
{
	// atributos
	private final static double PI = 3.141592;
	private double radio;
	// métodos
		// constructores
	public circulo() { radio = 1; }
	public circulo(double r) { radio = r; }
	// setters y getters
	public void setRadio(double r) { radio = r; }
	public double getRadio() { return radio; }
	// resto de métodos
	public double area() { return PI*radio*radio; }
	public double perimetro() { return 2*PI*radio; }
}


	
