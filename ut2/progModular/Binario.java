/* realiar dos funciones, una que convierta de binario a decimal, y el otro al contrario*/

import java.util.*;

public class Binario
{
	public static void main (String args[])
	{
		Scanner ent = new Scanner(System.in);
		long num;
		char in;

		System.out.println("introduce el número que deseas convertir");
		num=ent.nextLong();


		System.out.println('\n');


		System.out.println("A decimal? (INTRODUCE d) o a binario? (INTRODUCE b)");
		in = ent.next().charAt(0);


		System.out.println('\n');


		//ELECCIÓN DE CONVERSIÓN (NO SE HA CONTEMPLADO EL ERROR)

		switch (in)
			{
			case 'b' : System.out.println(abinario((int)num));break;
			case 'd' : System.out.println(adecimal(num));break;
			default: System.out.println("Error");break;

			}
		
		System.out.println('\n');
	}



		//FUNCIÓN INT DE CONVERSIÓN A BINARIO

	public static long abinario (int num)
	{
		int cont = 0, i = 1;
		long bin = 0;		/*LA VARIABLE cont ALMACENARÁ LA CANTIDAD DE DÍGITOS, 
		i SERÁ LA POTENCIA DEL ÍNDICE DE CADA DÍGITO, MIENTRAS QUE bin SERÁ LA VARIABLE A DEVOLER*/

		for (long large = num; large >= 1; large = large/2)  //BUCLE PARA CALCULAR CANTIDAD DE DÍGITOS
			cont++;
		for (int pw = cont; pw >=1; pw--)	//INSTRUCCIONES PARA HACER LA CONVERSIÓN
		{	
			bin = bin + (num%2)*i;
			num = num/2;
			i = i*10;
		}
		return bin;
   	 }

	public static int adecimal (long num)
	{
		int cont = 1, i = 1;
		int dec = 0;	/*LA VARIABLE cont ALMACENARÁ LA CANTIDAD DE DÍGITOS, 
		i SERÁ LA POTENCIA DEL ÍNDICE DE CADA DÍGITO, MIENTRAS QUE dec SERÁ LA VARIABLE A DEVOLER*/

		for (long large = num; large >= 10; large = large/10)	//BUCLE PARA CALCULAR CANTIDAD DE DÍGITOS
			cont++;
		for (int pw = cont; pw >=1; pw--)	//INSTRUCCIONES PARA HACER LA CONVERSIÓN
		{
			dec = dec + ((int)num)%10*i;
			num = num / 10;
			i = i*2;
		}
		return dec;
	}
	
}
