// 1. Realizar una función que reciba una variable de tipo caracter y si es una letra minúscula la devuelva convertida en mayúsculas. 

import java.util.Scanner;

public class ej1
{
	public static char min2May(char c)
	{
		// comprobar si c es minúscula (posciones entre 97 y 122)
		if ((c >= 97) && (c<= 122))
			return (char)(c - 32);
		else
			return c;
	}
	
	public static void main(String[] args) {
		char car;
		
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un caracter:");
		car = ent.next().charAt(0);
		car = min2May(car);
		System.out.println("El valor convertido a mayúscula es " + car);
		
		
	}
}
