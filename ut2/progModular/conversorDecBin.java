import java.util.Scanner;
public class conversorDecBin
{
	public static void main(String args[])
	{
        Scanner ent  = new Scanner(System.in);
    	System.out.println("Introduzca número decimal para pasarlo a binario:");
        int num= ent.nextInt();
        
    	System.out.println("El número en binario es: ");
       	decAbin(num);
       	
       	System.out.println("Introduzca número en binario para pasarlo a decimal:");
        num= ent.nextInt();

        System.out.println("Su correspondencia en decimal es " + binAdec(num));
	}

	public static void decAbin(int n)
	{
		if (n > 0)
		{
			decAbin(n/2);
    		System.out.print(n%2);
		}
    }
    
    public static int binAdec(int n)
    {
    	int dec=0,cont=0;
    	
    	while (n>0)
        {
            dec += (n%10) * Math.pow(2,cont);
            cont++;
            n/=10;
        }
    	return dec;
    }
}

