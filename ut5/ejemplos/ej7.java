import java.io.File;

public class ej7
{
	public static void main(String[] args)
	{
		File dir = new File("/home/rca/txt");
		
		if (dir.exists()) 	// si es true
		{
			System.out.println("Existe el directorio "+dir.getName());
			if (dir.canRead())
		   		System.out.println("El directorio existe y tiene permiso de lectura");
			if (dir.canWrite())
			   System.out.println("El directorio existe y tiene permiso de escritura");
			File[] ficheros = dir.listFiles();
			for (File f : ficheros)
				System.out.println(f.getName());
		}
		else	// si es false
			System.out.println("El directorio no existe");
	}
}
