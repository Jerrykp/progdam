import java.io.*;

public class ej9a
{
	public static void main(String args[])
	{
		double num;

		try
		{
			FileOutputStream fos=new FileOutputStream("nums.dat");
			DataOutputStream dos=new DataOutputStream(fos);
			System.out.println("Introduce un número (0 para acabar):");
			num=Double.parseDouble(System.console().readLine());
			while (num!=0)
			{
				dos.writeDouble(num);
				System.out.println("Introduce un número (0 para acabar):");
				num=Double.parseDouble(System.console().readLine());
			}
			dos.close();fos.close();
	
			FileInputStream fis=new FileInputStream("nums.dat");
			DataInputStream dis=new DataInputStream(fis);
			System.out.println("Los números leídos de fichero son:");
			num=dis.readDouble();
			while (num!=0)
			{
				System.out.println(num);
				num=dis.readDouble();
			}
			dis.close();fis.close();
		}
		catch (EOFException e)
		{
			System.out.println("Fin de fichero.");	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
