import java.io.*;

class amigo implements Serializable
{
	protected String nombre;
	protected long telefono;
	public amigo(String n, long t){
			nombre = n;
			telefono = t;
	}
	public void print(){
		System.out.println(nombre + " -> " + telefono+"\n");
	}
}

public class ej10
{
	public static void main(String[] args)
	{
		String []amigos={"Andrés Rosique","Pedro Ruiz","Isaac Sanchez","Juan Serrano"};
		long []telefonos={653364787,627463746,644567346,623746348};
		//escritura del fichero
		try{
			FileOutputStream fs = new FileOutputStream("amigos.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fs);
			for (int i=0;i<amigos.length;i++){
			// ¡OJO!: NECESARIAMENTE SE HAN DE INSTANCIAR TANTOS OBJETOS AMIGO (CON NEW) COMO AMIGOS QUIERA SERIALIZAR. NO ES VÁLIDO INSTANCIAR UN ÚNICO OBJETO AMIGO E IR MODIFICÁNDOLO PARA SERIALIZAR SIEMPRE EL MISMO OBJETO CON ATRIBUTOS DISTINTOS
				amigo a = new amigo(amigos[i],telefonos[i]);
				oos.writeObject(a);
			}
			if (oos != null){ 
				oos.close();
				fs.close();
			}
		}catch(IOException e){
		e.printStackTrace();
	}
	//lectura del fichero
	try{
		File f=null;
		FileInputStream fe = null;
		ObjectInputStream ois = null;
		try{
			f = new File("amigos.dat");
			if (f.exists()){
				fe = new FileInputStream(f);
				ois = new ObjectInputStream(fe);
				while(true){
					amigo a = (amigo)ois.readObject();
					a.print();
					//System.out.println("");
				} 				
			}
		}catch (EOFException eof) {
			System.out.println(" --------------------------");
		}catch (FileNotFoundException fnf) {
			System.err.println("Fichero no encontrado " + fnf);
		}catch(IOException e){
			System.err.println("Se ha producido una IOException");
			e.printStackTrace();
		}catch (Throwable e) {
			System.err.println("Error de programa: " + e);
			e.printStackTrace();
		}finally{
			if (ois != null) {
				ois.close();
				fe.close();
			}
		}
	}catch(IOException e){
		e.printStackTrace();
	}
}
}
