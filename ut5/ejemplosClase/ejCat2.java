/* Programa que muestre el contenido de un fichero pasado
 como parámetro utilizando BufferedReader (línea a línea)*/

import java.io.*;

public class ejCat2
{
	public static void main(String[] args) {
		if (args.length > 0)
		{
			String s;
			try
			{
				FileInputStream fis = new FileInputStream(args[0]);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				s = br.readLine();
				while (s != null)
				{
					System.out.println(s);
					s = br.readLine();
				}
				if (br != null)
				{
					br.close(); isr.close(); fis.close();
				}
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else
			System.out.println("Forma de uso: java ejCat2 fichero");
	}
}
