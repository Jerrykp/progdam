/* Programa que simule el comando wc (Word count)
utilizando BufferedReader (línea a línea)*/

import java.io.*;
//import java.util.StringTokenizer;
public class ejWc
{
	public static void main(String[] args) {
		if (args.length > 0)
		{
			String s; int contL,contP,contC;

			contP=contL=contC=0;
			try
			{
				FileInputStream fis = new FileInputStream(args[0]);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				s = br.readLine();
				while (s != null)
				{
					contL++;
					contC += s.length()+1;	// sumamos 1 por \n
					contP += s.split(" ").length;
					/* StringTonekiner st = new StringTokenizer(s);
					contP += st.countTokens(); */
					//System.out.println(s);
					s = br.readLine();
				}
				if (br != null)
				{
					br.close(); isr.close(); fis.close();
				}
				System.out.println(contL + " " + contP + " " + contC);
			}
			catch (IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else
			System.out.println("Forma de uso: java ejCat2 fichero");
	}
}