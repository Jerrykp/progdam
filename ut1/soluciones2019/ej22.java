// 22. Algoritmo que solicita notas introducidas por teclado acabadas con un numero negativo, e imprime en pantalla el aprobado de nota más baja y el suspenso de nota más alta.

import java.util.Scanner;

public class ej22{
	public static void main(String [] args){
		System.out.println("\n# Algoritmia - Ejercicio 22 #\n");

		Scanner ent = new Scanner(System.in);

		double nota;
		double aprobado = 11;
		double suspendido = -1;

		System.out.println("Introduce notas positivas entre 0 y 10 (negativa para finalizar el programa):");

		do{
			nota = ent.nextDouble();
			if (nota < 0)
				break;

			if ((nota < 5) && (nota > suspendido))
				suspendido = nota;
			if ((nota >= 5) && (nota < aprobado))
				aprobado = nota;
		} while ((nota >= 0) && (nota <= 10));

		if (aprobado != 11)
			System.out.println("\nAprobado con la nota más baja: " + aprobado);
		else
			System.out.println("\nNo has introducido ninguna nota aprobada");

		if (suspendido != -1)
			System.out.println("Suspendido con la nota más alta: " + suspendido);
		else
			System.out.println("No has introducido ninguna nota suspensa");

		System.out.println("\n# Fin del programa #\n");
		System.exit(0);
	}
}
