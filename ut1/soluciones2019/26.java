//Algoritmo que indique si un numero introducido por teclado es primo.
public class p17
{
    public static void main(String args[])
    {
        int num;
        boolean primo = true;
        
        System.out.println("Introduzca un número entero positivo");
        num = Integer.parseInt(System.console().readLine());
        if ( num > 0)
        {
            for (int a = 2; a <= num/2 ; a++)
                if ((num % a) == 0)
                {
                    primo = false;
                    break;
                }
            if (primo == true)
                System.out.println("El número es primo");
            else
                System.out.println("El número no es primo");
        }
        else	// cuando es negativo
        	System.out.println("El número no es válido");
    }
}
