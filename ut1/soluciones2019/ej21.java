//21. Programa que acepta números enteros mayores o iguales que 0 hasta acabar con un numero negativo e indique cuantos múltiplos de 2 se han introducido, cuantos  múltiplos de 3 y cuantos múltiplos de 2 y de 3 (de 6).

import java.util.Scanner;

public class ej21
{
    public static void main(String args[])
    {
     Scanner ent = new Scanner(System.in);
     int n,m2,m3,m6;
     System.out.println("Introduzca un número");
     m2=m3=m6=0;
 
     n = ent.nextInt();
     while (n>=0)
     {
      if ( n%6==0 )
       {
       	  m6++;m2++;m3++;
        }
      else // no es múltiplo de 6
        if ( n%2==0 )  // si es m2
          m2++;
        else
            if ( n%3==0) 
                m3++;
      System.out.println("Introduzca un número");
      n = ent.nextInt();
     } 
     System.out.println("Has introducido " + m2 +" números múltiplos de 2 ");
     System.out.println("Has introducido " + m3 +" números múltiplos de 3 ");
     System.out.println("Has introducido " + m6 +" números múltiplos de 6 ");
    }
}        

