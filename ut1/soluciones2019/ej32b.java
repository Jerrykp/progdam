/* 32. Realizar un programa que lea un entero del teclado y escriba en pantalla, en forma de cuadrado, la lista de números naturales desde el 1 hasta el número dado, rellenando los espacios sobrantes con 0.
	Ejemplo:
		Para el número 4:
				1 2 3 4
				2 3 4 0
				3 4 0 0
				4 0 0 0 */

import java.util.Scanner;

public class ej32b
{
   public static void main(String[] args)
   {
   	int num;
   	Scanner ent = new Scanner(System.in);
   	
   	System.out.println("Introduce un entero:");
   	num = ent.nextInt();
   	for (int i = 1; i <= num ; i++)
   	 {	// i será el índice de fila
		for (int j = i ; j < i + num ; j++ )
		{
			if (j <= num)
				System.out.print(j + " ");
			else
				System.out.print("0" + " ");
		}
		System.out.println("");
	}
		
		
   } 
}
