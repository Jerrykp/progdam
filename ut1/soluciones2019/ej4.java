//4. Programa que pide un valor numérico e indica si éste es positivo o negativo.
//import java.util.Scanner;
public class ej4
{
	public static void main(String []args)
	{
		double n1;

			//Scanner teclado= new Scanner(System.in);
			System.out.println("Introduce un valor numerico");
			//n1=teclado.nextDouble();
			n1 = Double.parseDouble(System.console().readLine());

			if( n1 > 0 )
				System.out.println("El valor introducido es positivo");
			else
				System.out.println("El valor introducido es negativo");
	}
}
