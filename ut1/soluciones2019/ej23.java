//Algoritmo que permite calcular el factorial de un numero entero.
import java.util.Scanner;

public class ej23
{
	public static void main(String args[])
	{
		Scanner ent = new Scanner(System.in);
		long num, fac;

		fac=1;

		System.out.println("Inserte un numero entero positivo para factorizar");
		num=ent.nextLong();
		while (num>1)
		{
			fac*=num;   // fac = fac*num
			num--;
		}
		if (num > 0)
            System.out.println(fac);	
	}
}
