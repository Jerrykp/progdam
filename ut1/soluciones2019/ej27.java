// 27*. Modificar el anterior algoritmo para que pida una lista de enteros acabada con un 0  e indique, para cada uno de ellos, si es primo o no. Además, al final ha de indicar cuántos primos se han introducido.

import java.util.Scanner;

public class ej27
{
    public static void main(String[] args)
    {
    	int num, contP=0; boolean esPrimo;
    	Scanner ent = new Scanner(System.in);
    	
    	System.out.println("Introduce un entero positivo:");
    	num = ent.nextInt();
    	while ( num != 0 )
    	{
    		esPrimo = true;
			for (int i=2 ; i <= num/2 ; i++)
				if (( num % i) == 0) // división exacta ==> NO es primo
				{
					//System.out.println("No es primo");
					esPrimo = false;
					break;
				}
			if (esPrimo)
			{
				// equivale a esPrimo == true
				System.out.println("Sí es primo");
				contP++;
			}
			else
				System.out.println("No es primo");
			System.out.println("Introduce un entero positivo:");
    		num = ent.nextInt();
		}
		System.out.println("Han sido " + contP + " primos");
     }
} 
