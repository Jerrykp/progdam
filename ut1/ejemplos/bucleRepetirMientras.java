// Programa que pida una nota (entre 0 y 10) obligando al usuario a reintroducir la nota tantas veces haga falta hasta que sea válida

public class bucleRepetirMientras
{
	public static void main(String args[]) {
		double nota;
		
		// bucle REPETIR-MIENTRAS
		do
		{
			System.out.println("Introduce una nota (entre 0 y 10):");
			nota = Double.parseDouble(System.console().readLine());
		}
		while ((nota < 0) || (nota > 10));	// nota NO válida
	}
}
