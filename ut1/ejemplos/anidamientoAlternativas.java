// Programa que pida una nota e indique su calificación

public class anidamientoAlternativas
{
	public static void main(String[] args) 
	{
		double n;
		
		System.out.println("Introduce tu nota de examen:");
		n = Double.parseDouble(System.console().readLine());
	while ( (n < 0) || (n > 10) )
	{
    	System.out.println("nota no válida, introduce una nota entre 0 y 10:");
        n = Double.parseDouble(System.console().readLine());
	}
		if ( n < 5 )
			System.out.println("Suspenso");
		else
			if ( n < 6 )
				System.out.println("Suficiente");
			else
				if ( n < 7 )
					System.out.println("Bien");
				else
					if ( n < 9 )
						System.out.println("Notable");
					else
						System.out.println("Sobresaliente");		
	}
}
