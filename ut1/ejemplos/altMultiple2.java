// Ejemplo de alternativa múltiple. El programa pedirá un entero (entre 1 y 7) y mostrará "día laborable" o "fin de semana"

public class altMultiple2
{
	public static void main(String[] args)
	{
		int dia;
		
		System.out.println("Escribe el día de la semana (entre 1 y 7):");
		dia = Integer.parseInt(System.console().readLine());
		switch (dia)
		{
			case 1: 
			case 2: 
			case 3: 
			case 4: 
			case 5: System.out.println("Día laborable");break;
			case 6: 
			case 7: System.out.println("Fin de semana");break;
			default: System.out.println("Valor incorrecto");
		}
			
		System.exit(0);
	}
}
