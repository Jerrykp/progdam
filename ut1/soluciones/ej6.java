//Programa que acepta 2 números y, si su suma es inferior a 10, pide un tercer valor. Al final muestra la suma de los valores introducidos.
import java.util.Scanner;

public class ej6
{
	public static void main(String args[])
	{
		Scanner ent =new Scanner(System.in);
		double n1,n2,n3,suma;

		System.out.println("Introduzca un numero");
		n1=ent.nextDouble();
		System.out.println("Introduzca otro numero para sumar");
		n2=ent.nextDouble();

		suma=n1+n2;	// La variable suma nos ayuda a no tener que poner n1+n2 cada vez que nos toca mostrar la suma

		if (suma <10)
			{
				System.out.println("Introduzca un tercer valor para sumar");
				n3=ent.nextDouble();
				suma += n3;	// añade n3 a la suma (suma = suma + n3)	
			}
//		else	
		System.out.println(suma);		

	}
}
