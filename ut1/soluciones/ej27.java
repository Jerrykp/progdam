//27*. Modificar el anterior algoritmo para que pida una lista de enteros acabada con un 0  e indique, para cada uno de ellos, si es primo o no. Además, al final ha de indicar cuántos primos se han introducido.

import java.util.*;

public class ej27
{
    public static void main (String args[])
    {
        int n, i, tope,contPr=0; boolean esPrimo;
        Scanner ent = new Scanner(System.in);
 
		System.out.println("Introduce un entero mayor que cero para conocer si es primo");
	    n = ent.nextInt();
		while ( n != 0)
		{   
			// mientras no encuentre un divisor es primo
			esPrimo=true;	
			tope = n/2;
		    for (i = 2; i <= tope; i++)
		       if (n%i==0)	// si la división es exacta
		       {
		        System.out.println("\nEl número introducido ("+n+") NO ES PRIMO\n");
		        esPrimo = false;
		        break;
		       }
			if (esPrimo)
			{
				contPr++;
		  		System.out.println("\nEl número introducido ("+n+") ES PRIMO\n");
			}
		  	System.out.println("Introduce un entero mayor que cero para conocer si es primo");
	    	n = ent.nextInt();
	    }
	    System.out.println("Han sido " + contPr + " números primos.");
    }
}
