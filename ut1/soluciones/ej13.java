// Realizar el ordinograma y pseudocódigo que lea una cantidad positiva/ negativa de ºC y la pase a grados Farenheit.

import java.util.*; // Se importa la clase Scanner

public class ej13
{
	public static void main(String[] args)
	{

		double grados, farenheit;

		Scanner entrada = new Scanner(System.in);  // Creación del objeto Scanner
		System.out.print("Introduce los grados centígrados:");
		grados = entrada.nextDouble(); // recogida de la entrada por teclado

		/*farenheit = (9.0/5.0)*grados;     // de esta forma me da bien pero si pongo (9/5) NO me da bien ¿por ser enteros?
		double resultado = farenheit + 32;*/
		farenheit = (1.8 * grados) + 32; // Fórmula para pasar de ºC a ºF. Al contrario sería C= F - 32/ 1.8

		System.out.println("Los grados centígrados " + grados + " en grados Farenheit equivale a " + farenheit);

	System.exit(0);


	}

}
